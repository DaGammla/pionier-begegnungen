const avgPlayerDmg = 14
const avgPlayerBlock = 4

const hpMin = 1
const hpMax = 500
const blockMin = 0
const blockMax = 12
const damageMin = 0
const damageMax = 20

const data = $fuwuProxy()
data.playerCount = 1
data.creatures = []
data.creatureMap = {}
data.savedCreatures = {}
data.difficulty = 0
data.loadDialogOpen = false

const storageData = window.localStorage.getItem("monsterData")
if (storageData != null && storageData.length > 0){
    const parsedData = JSON.parse(storageData)
    if (parsedData != null){
        for (const [key, value] of Object.entries(parsedData)) {
            if (key.startsWith("$"))
                continue
            
            data[key] = value
        }
    }
}

function copyCreature(creature){
    return {
        name: creature.name,
        hp: creature.hp,
        block: creature.block,
        throw: creature.throw,
        damage: creature.damage,
    }
}

function makeCreature(loadCreature){
    const creature = loadCreature ?? {
        name: "",
        hp: 30,
        block: 1,
        throw: 1,
        damage: 4,
        index: 0
    }
    let index = 1
    while (data.creatureMap[index] != null){
        index++
    }

    creature.index = index

    data.creatureMap[index] = creature
    return creature
}

function addNewCreature(){
    const c = makeCreature()
    data.creatures.push(c.index)
}

function getCreatureList(){
    return data.creatures.map((id) => data.creatureMap[id])
}

function cleanupCreatures(){
    for (let id of Object.keys(data.creatureMap)) {
        id = Number(id)
        const creature = data.creatureMap[id]
        if (!creature.saved && !data.creatures.includes(id)){
            data.creatureMap[id] = undefined
            delete data.creatureMap[id]
        }
    }
}

function saveCreature(creature){

    if (creature.name.length == 0){
        window.alert("Die Kreatur benötigt einen Namen")
        return
    }

    const copy = copyCreature(creature)

    data.savedCreatures[creature.name] = copy
}

function addSavedCreature(creature){
    const loaded = makeCreature(copyCreature(creature))
    data.creatures.push(loaded.index)
}

function deleteSavedCreature(creature){
    data.savedCreatures[creature.name] = undefined
    delete data.savedCreatures[creature.name]
}

const throwMap = {
    0: [1, 4.5, 8, "D8"],
    1: [1, 6.5, 12, "D12"],
    2: [1, 10.5, 20, "D20"],
    3: [2, 15, 28, "D20 + D8"]
}

function getThrowAverage(dice){
    return throwMap[dice][1]
}

function getThrowHigh(dice){
    return throwMap[dice][2]
}

function getThrowLow(dice){
    return throwMap[dice][0]
}

function getThrowLabel(dice){
    return throwMap[dice][3]
}

function calculate(){

    data.difficulty = 0

    const creatures = getCreatureList()

    const playerCreatureRatio = data.playerCount / creatures.length
    const ratioAffect = (0.85 * playerCreatureRatio + 0.15) + (data.playerCount - 1) * 0.15

    for (const creature of creatures) {
        creature.surviveRounds = (creature.hp / (avgPlayerDmg - creature.block)) / ratioAffect
        creature.damagePerRound = getThrowAverage(creature.throw) + Number(creature.damage)
        creature.maxDamage = getThrowHigh(creature.throw) + Number(creature.damage)
        creature.minDamage = getThrowLow(creature.throw) + Number(creature.damage)

        creature.lifetimeDamage = creature.surviveRounds * (creature.damagePerRound - avgPlayerBlock)
        data.difficulty += creature.lifetimeDamage * 1.0
    }

    data.difficulty = Math.round(data.difficulty / data.playerCount)

    cleanupCreatures()
    window.localStorage.setItem("monsterData", JSON.stringify(data))
}

data.$listeners.push(calculate)
calculate()

function random(difficulty){
    const thr = Math.floor(Math.random() * 4)
    const avgThrow = getThrowAverage(thr)
    const randomPick = Math.floor(Math.random() * 3)

    let block
    let damageBase
    let hp

    if (randomPick == 0){
        block = difficulty * 9 * Math.random()
        damageBase = difficulty * 12 * Math.random()
        hp = ( difficulty * 125 * (avgPlayerDmg - block) ) / (damageBase + avgThrow - avgPlayerBlock)
    } else if (randomPick == 1) {
        damageBase = difficulty * 12 * Math.random()
        hp = 60 * difficulty * Math.random()
        block = -hp * (damageBase + avgThrow - avgPlayerBlock) / (difficulty * 100) + avgPlayerDmg
    } else {
        hp = 60 * difficulty * Math.random()
        block = difficulty * 9 * Math.random()
        damageBase = (-125 * block * difficulty + hp*(avgPlayerBlock-avgThrow) + 100 * avgPlayerDmg * difficulty) / hp
    }

    data.block = Math.round(block)
    data.damageBase = Math.round(damageBase)
    data.hp = Math.round(hp)
    data.damageThrow = thr
}